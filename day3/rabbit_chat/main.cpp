#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <thread>
#include <string>

using namespace std;
using namespace AmqpClient;

const string addr = "test.czterybity.pl";
const int port = 5672;
const string ex_name = "luxoft";

void send()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, "admin", "tymczasowe");

    for (;;)
    {
        string msg;
        cout << ">>> " << endl;
        getline(cin, msg);
        msg = "[Leszek] " + msg;
        channel->BasicPublish(ex_name, "", BasicMessage::Create(msg));
    }
}

void read_msg()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, "admin", "tymczasowe");

    string q_name = channel->DeclareQueue("");
    channel->BindQueue(q_name, ex_name, "");
    channel->BasicConsume(q_name);
    for(;;)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main()
{
    thread th1(&send);
    thread th2(&read_msg);

    th1.join();
    th2.join();

    return 0;
}

