#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>
#include "smallpt_lib.hpp"

using namespace std;

int main()
// scheme:
// Leszek: 123 :: rgb rgb
{
    cout << "Hello zmq render worker" << endl;
    zmq::context_t context(1);
    zmq::socket_t soc_in(context, ZMQ_REQ);
    zmq::socket_t soc_out(context, ZMQ_PUSH);
    zmq::socket_t soc_kill(context, ZMQ_SUB);

    srand(time(NULL));
    s_set_id(soc_in);

    soc_in.connect("tcp://test.czterybity.pl:8888");
    soc_out.connect("tcp://test.czterybity.pl:9999");
    soc_kill.connect("tcp://test.czterybity.pl:9191");
    soc_kill.setsockopt(ZMQ_SUBSCRIBE, 0, 0);

    cout << "connected!" << endl;

    zmq::pollitem_t items [] = {
        { soc_in, 0, ZMQ_POLLIN, 0 },
        { soc_kill, 0, ZMQ_POLLIN, 0 }
    };

    s_send(soc_in, "Leszek asks for line number");

    for(;;)
    {
        zmq::poll (&items [0], 2, -1);

        if (items [0].revents & ZMQ_POLLIN)
        {

            cout << "req send... " << endl;
            auto line_str = s_recv(soc_in);
            cout << "got: " << line_str;
            istringstream iss(line_str);
            int line_no;
            int quality;
            iss >> line_no >> quality;
            auto line = scan_line(line_no, quality);
            string output;
            output = string("Leszek: ") + to_string(line_no)
                    + " :: " + line;

            s_send(soc_out, output);
            cout << "... finish" << endl;
            s_send(soc_in, "Leszek asks for line number");
        }

        if (items [1].revents & ZMQ_POLLIN)
        {
            //  Any waiting controller command acts as 'KILL'
            std::cout << "got KILL from sink" << std::endl;
            break;                      //  Exit loop
        }
    }
    // clean up
    return 0;
}
