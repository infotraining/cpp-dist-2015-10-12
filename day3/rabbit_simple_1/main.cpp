#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <thread>
#include <string>

using namespace std;
using namespace AmqpClient;

const string addr = "test.czterybity.pl";
const int port = 5672;
const string ex_name = "luxoft";
const string q_name = "qluxoft";

void send()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, "admin", "tymczasowe");
    channel->DeclareExchange(ex_name, Channel::EXCHANGE_TYPE_DIRECT);

    channel->DeclareQueue(q_name, false, false, false, false);
    channel->BindQueue(q_name, ex_name,  "");

    for (int i = 0 ; i < 1000 ; ++i)
    {
        channel->BasicPublish(ex_name, "", BasicMessage::Create("hello " + to_string(i)));
        this_thread::sleep_for(chrono::milliseconds(200));
    }
}

void read_msg()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, "admin", "tymczasowe");

    channel->BasicConsume(q_name, "", true, true, false);
    for(;;)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main()
{
    thread th1(&send);
    thread th2(&read_msg);
    thread th3(&read_msg);
    th1.join();
    th2.join();
    th3.join();
    return 0;
}

