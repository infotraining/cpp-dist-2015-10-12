#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <thread>
#include <string>

using namespace std;
using namespace AmqpClient;

const string addr = "test.czterybity.pl";
const int port = 5672;
const string ex_inp = "x_numbers";
const string q_input = "q_numbers";
const string ex_res = "x_primes";
const string q_res = "q_primes";

void setup()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, "admin", "tymczasowe");
    channel->DeclareExchange(ex_inp, Channel::EXCHANGE_TYPE_DIRECT);
    channel->DeclareExchange(ex_res, Channel::EXCHANGE_TYPE_DIRECT);

    channel->DeclareQueue(q_input, false, false, false, false);
    channel->DeclareQueue(q_res, false, false, false, false);

    channel->BindQueue(q_input, ex_inp,  "");
    channel->BindQueue(q_res, ex_res,  "");
}

void produce()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, "admin", "tymczasowe");

    for(long i = 10'000'000 ; i < 10'100'000 ; ++i)
    {
        channel->BasicPublish(ex_inp, "" , BasicMessage::Create(to_string(i)));
    }
}

void printer()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, "admin", "tymczasowe");
    channel->BasicConsume(q_res, "", true, true, false);
    for(;;)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main()
{
    //setup();
    thread th1(produce);
    thread th2(printer);
    th1.join();
    th2.join();
    return 0;
}

