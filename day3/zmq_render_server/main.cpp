#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>
#include <string>
#include <vector>

using namespace std;

int main()
{
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_ROUTER);
    socket.bind("tcp://*:9999");
    //for (;;)
        for(int i = 0 ; i < 768 ; ++i)
        {
            // handshake
            string address = s_recv(socket);
            s_recv(socket);
            string client_name = s_recv(socket);
            cout << "request from " << address << " " << client_name << endl;
            // sending
            s_sendmore(socket, address);
            s_sendmore(socket, "");
            s_send(socket, to_string(i) + " " + to_string(128));
        }
}

