#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>
#include <string>
#include <vector>

using namespace std;

zmq::context_t context(1);

void producer()
{
    zmq::socket_t socket(context, ZMQ_PUSH);
    socket.bind("tcp://*:5555");
    for(int i = 0 ; i < 100 ; ++i)
    {
        this_thread::sleep_for(100ms);
        s_send(socket, "message " + to_string(i));
    }
}

void worker(int id)
{
    zmq::socket_t socket(context, ZMQ_PULL);
    socket.connect("tcp://localhost:5555");
    for(;;)
    {
        cout << id << " got " << s_recv(socket) << endl;
    }
}

int main()
{
    vector<thread> thds;
    thds.emplace_back(producer);
    thds.emplace_back(worker, 1);
    thds.emplace_back(worker, 2);
    for (auto &t : thds) t.join();
    return 0;
}

