#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>
#include <string>

using namespace std;

int main()
{
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_SUB);
    socket.connect("tcp://test.czterybity.pl:5555");
    socket.setsockopt(ZMQ_SUBSCRIBE, 0, 0);

    for(;;)
    {
        cout << "just got: " << s_recv(socket) << endl;
    }

    return 0;
}

