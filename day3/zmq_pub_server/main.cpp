#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>
#include <string>

using namespace std;

int main()
{
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_PUB);
    socket.bind("tcp://*:5555");

    for(long i = 0 ; i < 100000000 ; ++i )
    {
        this_thread::sleep_for(1000ms);
        s_send(socket, "message broadcasted" + to_string(i));
    }

    return 0;
}

