#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <thread>
#include <string>

using namespace std;
using namespace AmqpClient;

const string addr = "test.czterybity.pl";
const int port = 5672;
const string ex_inp = "x_numbers";
const string q_input = "q_numbers";
const string ex_res = "x_primes";
const string q_res = "q_primes";

bool is_prime(long n)
{
    for (long div = 2 ; div < n ; ++div)
        if( div % n == 0) return false;
    return true;
}

int main()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, "admin", "tymczasowe");
    channel->BasicConsume(q_input, "", true, false, false, 1);
    for(;;)
    {
        Envelope::ptr_t env = channel->BasicConsumeMessage();
        long n = stol(env->Message()->Body());
        if (is_prime(n))
            channel->BasicPublish(ex_res, "",
                      BasicMessage::Create(string("Leszek ") + to_string(n)));
        channel->BasicAck(env);
    }
    return 0;
}

