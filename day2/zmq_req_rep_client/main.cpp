#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>

using namespace std;

int main()
{
    cout << "Hello Req-rep client!" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REQ);
    socket.connect("tcp://test.czterybity.pl:6666");

    for(int i = 0 ; i < 100000; ++i )
    {
        this_thread::sleep_for(1000ms);
        s_send(socket, "this is Leszeks client");
        cout << "got msg: " << s_recv(socket) << endl;
    }

    return 0;
}

