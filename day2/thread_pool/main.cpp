#include <iostream>
#include <functional>
#include "thread_safe_queue.h"

using namespace std;

void myfun()
{
    cout << "what a fun!" << endl;
}

using task_t = std::function<void()>;

class thread_pool
{
    thread_safe_queue<task_t> q_;
    vector<thread> workers;
public:
    thread_pool(size_t n_of_workers)
    {
        // create n threads/workers
        for (int i ; i < n_of_workers ; ++i)
            workers.emplace_back(
                [this] {
                for(;;)
                {
                    task_t task;
                    q_.pop(task);
                    if(!task) return;
                    task();
                }
            });
    }

    void submit(task_t f)
    {
        if(f)
            q_.push(f);
    }

    ~thread_pool()
    {
        // wait for workers!!
        for(auto& worker : workers) q_.push(task_t());
        for(auto& worker : workers) worker.join();
    }
};

int main()
{
//    std::function<void()> f;
//    //f();
//    f = myfun;
//    f = [] { cout << "lambda on duty" << endl;};
//    if(f)
//        f();

    thread_pool tp(8);
    tp.submit(myfun);
    tp.submit([] { cout << "lambda on duty" << endl;});
    for(int i = 0 ; i < 100 ; ++i)
    {
        tp.submit([i] {
            cout << "lambda nr " << i << endl;
            this_thread::sleep_for(500ms);
        });
    }

    return 0;
}

