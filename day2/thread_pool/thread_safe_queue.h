#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H

#include <iostream>
#include <queue>
#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>

template<typename T>
class thread_safe_queue
{
    std::queue<T> q_;
    std::mutex mtx_;
    std::condition_variable cond_;
public:
    void pop(T& item)
    {
        std::unique_lock<std::mutex> lg(mtx_);
        while(q_.empty()) cond_.wait(lg);
        item = q_.front();
        q_.pop();
    }

    bool pop_nowait(T& item)
    {
        std::lock_guard<std::mutex> lg(mtx_);
        if(q_.empty()) return false;
        item = q_.front();
        q_.pop();
        return true;
    }

    void push(T item)
    {
        std::lock_guard<std::mutex> lg(mtx_);
        q_.push(item);
        cond_.notify_one();
    }
};

#endif // THREAD_SAFE_QUEUE_H
