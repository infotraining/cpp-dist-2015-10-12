#include <iostream>
#include <queue>
#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>

using namespace std;

queue<int> q;
mutex qmtx;
condition_variable cond;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        {
            lock_guard<mutex> lg(qmtx);
            q.push(i);
            cond.notify_one();
        }
        this_thread::sleep_for(chrono::milliseconds(10));
    }
}

void consumer(int id)
{
    for(;;)
    {
        unique_lock<mutex> lg(qmtx);
        while(q.empty())
        {
            cond.wait(lg); // inside -> lg.unlock, wait, lg.lock
        }
        //cond.wait(lg, [] { return !q.empty();});
        int msg = q.front();
        q.pop();
        lg.unlock();
        cout << id << " just got " << msg << endl;

    }
}

int main()
{
    vector<thread> thds;
    thds.emplace_back(producer);
    thds.emplace_back(consumer,1);
    thds.emplace_back(consumer,2);
    for(auto& t: thds) t.join();
    return 0;
}

