#include <iostream>
#include <future>
#include <thread>
#include <stdexcept>

using namespace std;

int question(int id)
{

    this_thread::sleep_for(5s);
    if (id == 13) throw std::logic_error("bad luck");
    return 42;
}

int main()
{

    //future<int> res = std::async(launch::async, question, 13);
    packaged_task<int(int)> pt(question);
    future<int> res = pt.get_future();
    thread  th(move(pt), 10);
    th.detach();

    cout << "started task" << endl;
    res.wait();
    cout << "end of waiting" << endl;
    try
    {
        cout << "Result = " << res.get() << endl;
    }
    catch(std::logic_error& err)
    {
        cerr << "logic error " << err.what() << endl;
    }

    return 0;
}

