#include <iostream>
#include <queue>
#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>

using namespace std;

template<typename T>
class thread_safe_queue
{
    std::queue<T> q_;
    std::mutex mtx_;
    std::condition_variable cond_;
public:
    void pop(T& item)
    {
        std::unique_lock<std::mutex> lg(mtx_);
        while(q_.empty()) cond_.wait(lg);
        item = q_.front();
        q_.pop();
    }

    bool pop_nowait(T& item)
    {
        std::lock_guard<std::mutex> lg(mtx_);
        if(q_.empty()) return false;
        item = q_.front();
        q_.pop();
        return true;
    }

    void push(T item)
    {
        std::lock_guard<std::mutex> lg(mtx_);
        q_.push(item);
        cond_.notify_one();
    }
};

thread_safe_queue<int> q;
condition_variable cond;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        q.push(i);
        this_thread::sleep_for(chrono::milliseconds(10));
    }
}

void consumer(int id)
{
    for(;;)
    {
        int msg;
        //if (!q.pop_nowait(msg)) continue;
        q.pop(msg);
        cout << id << " just got " << msg << endl;
    }
}

int main()
{
    vector<thread> thds;
    thds.emplace_back(producer);
    thds.emplace_back(consumer,1);
    thds.emplace_back(consumer,2);
    for(auto& t: thds) t.join();
    return 0;
}

