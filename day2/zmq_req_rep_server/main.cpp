#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>

using namespace std;

int main()
{
    cout << "Hello Req-rep server!" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REP);
    socket.bind("tcp://*:6666");

    for(;;)
    {
        cout << "got request " << s_recv(socket) << endl;

        s_send(socket, "msg from server");
    }

    return 0;
}

