#include <iostream>
#include <thread>
#include <atomic>
#include <mutex>

using namespace std;

atomic<long> counter(0);

void increase()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        counter.fetch_add(1);
    }
}

long mcounter = 0;
mutex mtx;

void increase_mtx()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        mtx.lock(); // <-- blocks if mutex is used elsewhere
        ++mcounter;
        mtx.unlock();
    }
}

int main()
{
    cout << "Hello World!" << endl;

    cout << "is lock free? " << counter.is_lock_free() << endl;

    // timer start
    auto start = chrono::high_resolution_clock::now();

    thread th1(increase);
    thread th2(increase);
    th1.join();
    th2.join();

    //timer end
    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    cout << "Counter = " << counter << endl;

    // timer start
     start = chrono::high_resolution_clock::now();

    thread th3(increase_mtx);
    thread th4(increase_mtx);
    th3.join();
    th4.join();

    //timer end
     end = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    cout << "Counter mtx = " << mcounter << endl;
    return 0;
}

