#include <iostream>
#include <mutex>
#include <vector>
#include <thread>

using namespace std;

long mcounter = 0;
timed_mutex mtx;

void increase_mtx()
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        lock_guard<timed_mutex> lg(mtx);
        ++mcounter;
        if (mcounter == 100)
            return;
    }
}

void worker(int id)
{
    cout << "Starting worker " << id << endl;
    unique_lock<timed_mutex> ul(mtx, try_to_lock);
    if (!ul.owns_lock())
    {
        cout << id << " didn't get the lock :(" << endl;
        while(!ul.try_lock_for(500ms))
        {
            cout << "waiting..." << endl;
        }
    }
    cout << id << " has lock!" << endl;
    this_thread::sleep_for(2s);

}

int main()
{
    cout << "Hello World!" << endl;
    thread th1(worker, 1);
    thread th2(worker, 2);
    th1.join();
    th2.join();
    cout << "Counter = " << mcounter << endl;
    return 0;
}

