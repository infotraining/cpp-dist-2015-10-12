#include <iostream>
#include <random>
#include <chrono>
#include <thread>
#include <vector>

using namespace std;

void calc_stat(long total, long& hits)
{
    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_real_distribution<> dis(0,1);
    long counter = 0;
    for (int n = 0; n < total; ++n)
    {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            ++counter;
    }
    hits = counter;
}

int main()
{
    const long total = 100000000;

    int N_THREADS = 1;
    vector<long> counters(N_THREADS);
    vector<thread> workers;

    // timer start
    auto start = chrono::high_resolution_clock::now();

    for(int i = 0 ; i < N_THREADS ; ++i)
    {
        workers.emplace_back(calc_stat, total/N_THREADS, ref(counters[i]));
    }

    for(auto& worker : workers) worker.join();

    cout << "Pi = " << double(accumulate(counters.begin(), counters.end(), 0L))/double(total)*4;
    cout << endl;


    //timer end
    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
    return 0;
}

