#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void fun(int id)
{
    cout << "another thread " << id << endl;
    this_thread::sleep_for(chrono::seconds(1));
}

void calculate(int question, int& answer)
{
    cout << "calculating" << endl;
    this_thread::sleep_for(100ms);
    answer = question * question;
    cout << "DEBUG: " << answer << endl;
}

thread generate_thread()
{
    int answer = 0;
    // dangling ref, don't do that
    thread th(calculate, 10, ref(answer));
    return th;
}

int main()
{
    int a = 100;
    cout << "Hello World!" << endl;
    thread th(&fun, a);
    cout << "after launching thread" << endl;
    th.join();
    cout << "After thread" << endl;

    // calculating
    int answer = 0;
    thread th2(&calculate, 10, ref(answer));
    th2.join();
    cout << "Answer = " << answer << endl;

    //thread th3 = th; no copy ctor
    // but we have move semantic

    vector<thread> thds;
    thds.push_back(thread(fun, 11));
    thds.emplace_back(fun, 12);
    thds.push_back(generate_thread());

    for(auto& th : thds) th.join();

    return 0;
}

